﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ConnectionPointType { In, Out }

public class ConnectionPoint
{
    private Rect _connectionPointRect;

    public Rect ConnectionPointRect
    {
        get { return _connectionPointRect; }
    }

    public ConnectionPointType PointType { get; private set; }
    public DialogueNode Node { get; private set; }
    public GUIStyle Style { get; private set; }

    public Action<ConnectionPoint> OnClickConnectionPoint { get; private set; }

    public ConnectionPoint(DialogueNode node, ConnectionPointType pointType, GUIStyle style, Action<ConnectionPoint> onClickConnectionPoint)
    {
        Node = node;
        PointType = pointType;
        Style = style;
        OnClickConnectionPoint = onClickConnectionPoint;
        _connectionPointRect = new Rect(0, 0, 10, 20);
    }

    public void Draw()
    {
        _connectionPointRect.y = Node.DialogueRect.y + (Node.DialogueRect.height * 0.5f) - _connectionPointRect.height * 0.5f;
        switch (PointType)
        {
            case ConnectionPointType.In:
                _connectionPointRect.x = Node.DialogueRect.x - _connectionPointRect.width + 8f;
                break;
            case ConnectionPointType.Out:
                _connectionPointRect.x = Node.DialogueRect.x - _connectionPointRect.width + 201f; //TODO: commit sudoku
                break;
        }

        if (OnClickConnectionPoint != null && GUI.Button(_connectionPointRect, String.Empty, Style))
        {
            OnClickConnectionPoint(this);
        }
    }
}
