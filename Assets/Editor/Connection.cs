﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Connection
{
    public ConnectionPoint InPoint;
    public ConnectionPoint OutPoint;
    public Action<Connection> OnClickRemoveConnection;

    public Connection(ConnectionPoint inPoint, ConnectionPoint outPoint, Action<Connection> onClickRemoveConnection)
    {
        InPoint = inPoint;
        OutPoint = outPoint;
        OnClickRemoveConnection = onClickRemoveConnection;
    }

    public void Draw()
    {
        Handles.DrawBezier(InPoint.ConnectionPointRect.center, OutPoint.ConnectionPointRect.center, InPoint.ConnectionPointRect.center + Vector2.left * 50f, InPoint.ConnectionPointRect.center - Vector2.left * 50f, Color.white, null, 2f);

        if (Handles.Button((InPoint.ConnectionPointRect.center + OutPoint.ConnectionPointRect.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap))
        {
            if (OnClickRemoveConnection != null)
            {
                OnClickRemoveConnection(this);
            }
        }
    }
}
