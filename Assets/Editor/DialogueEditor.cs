﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DialogueEditor : EditorWindow
{
    private List<DialogueNode> _nodes;
    private GUIStyle _nodeStyle;
    private GUIStyle _inPointStyle;
    private GUIStyle _outPointStyle;
    private ConnectionPoint _selectedOutPoint;
    private ConnectionPoint _selectedInPoint;
    private List<Connection> _connections;
    private GUIStyle _selectedNodeStyle;
    private Vector2 _drag;

    [MenuItem("Window/Dialogue Tree Editor")]
    private static void OpenWindow()
    {
        var window = GetWindow<DialogueEditor>();
        window.titleContent = new GUIContent("Dialogue Tree Editor");
    }

    private void OnEnable()
    {
        if (_nodes == null)
        {
            _nodes = new List<DialogueNode>();
        }
        if (_connections == null)
        {
            _connections = new List<Connection>();
        }
        _nodeStyle = new GUIStyle
        {
            normal = { background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D },
            border = new RectOffset(12, 12, 12, 12)
        };
        _selectedNodeStyle = new GUIStyle
        {
            normal = { background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1 on.png") as Texture2D },
            border = new RectOffset(12, 12, 12, 12)
        };
        _inPointStyle = new GUIStyle
        {
            normal = { background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left.png") as Texture2D },
            active = { background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left on.png") as Texture2D },
            border = new RectOffset(4, 4, 12, 12)
        };
        _outPointStyle = new GUIStyle
        {
            normal = { background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right.png") as Texture2D },
            active = { background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right on.png") as Texture2D },
            border = new RectOffset(4, 4, 12, 12)
        };


    }

    private void OnGUI()
    {
        DrawDialogueNodes();
        DrawConnections();
        DrawConnectionLine(Event.current);
        ProcessDialogueNodeEvents(Event.current);
        ProcessEvents(Event.current);
        if (GUI.changed) Repaint();
    }

    private void DrawConnectionLine(Event current)
    {
        if (_selectedInPoint != null && _selectedOutPoint == null)
        {
            Handles.DrawBezier(_selectedInPoint.ConnectionPointRect.center, current.mousePosition, _selectedInPoint.ConnectionPointRect.center + Vector2.left * 50f, current.mousePosition - Vector2.left * 50f, Color.white, null, 2f);
            GUI.changed = true;
        }
        else if (_selectedInPoint == null && _selectedOutPoint != null)
        {
            Handles.DrawBezier(_selectedOutPoint.ConnectionPointRect.center, current.mousePosition, _selectedOutPoint.ConnectionPointRect.center + Vector2.left * 50f, current.mousePosition - Vector2.left * 50f, Color.white, null, 2f);
            GUI.changed = true;
        }
    }

    private void DrawConnections()
    {
        foreach (var connection in _connections)
        {
            connection.Draw();
        }
    }

    private void ProcessDialogueNodeEvents(Event current)
    {
        foreach (var dialogueNode in _nodes)
        {
            bool guiChanged = dialogueNode.ProcessEvents(current);
            if (guiChanged)
            {
                GUI.changed = true;
            }
        }
    }

    private void ProcessEvents(Event current)
    {
        _drag = Vector2.zero;
        switch (current.type)
        {
            case EventType.MouseDown:
                if (current.button == 1)
                {
                    GenericMenu rightClickMenu = new GenericMenu();
                    rightClickMenu.AddItem(new GUIContent("Add node"), false, () => OnClickAddNode(current.mousePosition));
                    rightClickMenu.ShowAsContext(); //TODO: commit sudoku later
                }
                break;
            case EventType.MouseDrag:
                if (current.button == 0)
                {
                    OnDrag(current.delta);
                }
                break;
        }
    }

    private void OnDrag(Vector2 currentDelta)
    {
        _drag = currentDelta;
        foreach (var dialogueNode in _nodes)
        {
            dialogueNode.DragNode(_drag);
        }
    }

    private void OnClickAddNode(Vector2 currentMousePosition)
    {
        _nodes.Add(new DialogueNode(currentMousePosition, new Vector2(200, 50), _nodeStyle, _selectedNodeStyle, _inPointStyle, _outPointStyle, OnClickInPoint, OnClickOutPoint));
    }

    private void OnClickOutPoint(ConnectionPoint outPoint)
    {
        _selectedOutPoint = outPoint;
        if (_selectedInPoint != null && _selectedInPoint.Node != _selectedOutPoint.Node)
        {
            CreateConnection();
            ClearConnectionSelection();
        }

    }

    private void CreateConnection()
    {
        _connections.Add(new Connection(_selectedInPoint, _selectedOutPoint, OnClickRemoveConnection));
    }

    private void OnClickRemoveConnection(Connection con)
    {
        _connections.Remove(con);
    }

    private void ClearConnectionSelection()
    {
        _selectedInPoint = null;
        _selectedOutPoint = null;
    }

    private void OnClickInPoint(ConnectionPoint inPoint)
    {
        _selectedInPoint = inPoint;
        if (_selectedOutPoint != null && _selectedInPoint.Node != _selectedOutPoint.Node)
        {
            CreateConnection();
            ClearConnectionSelection();
        }

    }

    private void DrawDialogueNodes()
    {
        foreach (var dialogueNode in _nodes)
        {
            dialogueNode.Draw();
        }
    }
}
