﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueNode
{
    public Rect DialogueRect;
    public string DialogueNodeTitle;
    public GUIStyle NodeStyle;
    private bool _isDragged;

    public ConnectionPoint InPoint;
    public ConnectionPoint OutPoint;
    private bool _isSelected;
    private GUIStyle _selectedNodeStyle;
    private GUIStyle _defaultNodeStyle;

    public DialogueNode(Vector2 pos, Vector2 widthHeight, GUIStyle defaultNodeStyle, GUIStyle selectedNodeStyle, GUIStyle inPointStyle, GUIStyle outPointStyle, Action<ConnectionPoint> onClickInPoint, Action<ConnectionPoint> onClickOutPoint)
    {
        DialogueRect = new Rect(pos.x, pos.y, widthHeight.x, widthHeight.y);
        _defaultNodeStyle = defaultNodeStyle;
        _selectedNodeStyle = selectedNodeStyle;
        NodeStyle = _defaultNodeStyle;
        InPoint = new ConnectionPoint(this, ConnectionPointType.In, inPointStyle, onClickInPoint);
        OutPoint = new ConnectionPoint(this, ConnectionPointType.Out, outPointStyle, onClickOutPoint);
    }

    public void DragNode(Vector2 deltaVect)
    {
        DialogueRect.position += deltaVect;
    }

    public void Draw()
    {
        InPoint.Draw();
        OutPoint.Draw();
        GUI.Box(DialogueRect, DialogueNodeTitle, NodeStyle);
    }

    public bool ProcessEvents(Event e) //TODO: wut
    {
        switch (e.type)
        {
            case EventType.MouseDown:
                if (e.button == 0)
                {
                    if (DialogueRect.Contains(e.mousePosition))
                    {
                        _isDragged = true;
                        GUI.changed = true;
                        _isSelected = true;
                        NodeStyle = _selectedNodeStyle;
                    }
                    else
                    {
                        GUI.changed = true;
                        _isSelected = false;
                        NodeStyle = _defaultNodeStyle;
                    }
                }
                break;

            case EventType.MouseUp:
                _isDragged = false;
                break;

            case EventType.MouseDrag:
                if (e.button == 0 && _isDragged)
                {
                    DragNode(e.delta);
                    e.Use();
                    return true;
                }
                break;
        }
        return false;
    }
}
